from unicodedata import name
from django.db import models
from django.utils.safestring import mark_safe

import random

# Create your models here.

'''
课程管理
'''


class CurriculumModel(models.Model):
    title = models.CharField(u'课程名称', max_length=64)
    cover_pic = models.ImageField(u'封面图', max_length=64, upload_to='images/', blank=True)
    suitable_people = models.CharField(u'适合人群', max_length=64)
    course_plan = models.CharField(u'排课计划', max_length=64)
    TOOLS = ((0, u'图形化'), (1, u'C++'), (2, u'Python'), (3, u'通用课程'))
    course_tools = models.IntegerField(u'课程工具', choices=TOOLS)
    course_number = models.CharField(u'课程数', max_length=64)
    course_sections = models.CharField(u'小节数', max_length=64)
    sort = models.CharField(u'排序', max_length=64)
    created_at = models.DateTimeField(u'创建时间', auto_now_add=True)
    updated_at = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return u"%s" % self.title

    def __str__(self):
        return u"%s" % self.title

    def image_data(self):
        if self.cover_pic:
            return mark_safe(
                '<img src="/media/%s" width="440px" height="275px"/>' % self.cover_pic)
        else:
            return mark_safe(
                '<img src="/media/无拍照上传.png" width="156px" height="98px"/>')

    # 填写表单里面并没有配置图片，可以根据自己的需求修改配置
    def image_admin(self):
        return mark_safe(
            '<img src="/media/%s" width="440px" height="275px"/>' % self.cover_pic)

    cover_pic.short_description = '封面图'
    cover_pic.short_description = '封面图'

    class Meta:
        db_table = "curriculum"
        ordering = ('created_at',)
        verbose_name = "课程管理"
        verbose_name_plural = "课程管理"


'''
教师管理
'''


class Teacher(models.Model):
    name = models.CharField(u'姓名', max_length=30)
    mobile = models.CharField(u'手机号', max_length=11)
    pwd = models.CharField(u'密码', max_length=15)
    IS_STATE_CHOICES = ((0, u'离职'), (1, u'在职'))
    is_state = models.IntegerField(u'状态', choices=IS_STATE_CHOICES)
    created_at = models.DateTimeField(u'创建时间', auto_now_add=True)
    updated_at = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    def __str__(self):
        return u"%s" % self.name

    class Meta:
        db_table = "teacher"
        ordering = ('created_at',)
        verbose_name = "教师管理"
        verbose_name_plural = "教师管理"


'''
班级管理
'''


class ClassRoom(models.Model):
    name = models.CharField(u'班级名称', max_length=64)
    IS_AUDIT_CHOICES = ((0, u'不审核'), (1, u'审核'))
    is_audit = models.IntegerField(u'学生加入', choices=IS_AUDIT_CHOICES)
    invitation_code = models.CharField(u'邀请码', max_length=32)
    teacher = models.ManyToManyField("Teacher", verbose_name=u'教师')
    restrict = models.IntegerField(u'人数限制')
    created_at = models.DateTimeField(u'创建时间', auto_now_add=True)
    updated_at = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    def __str__(self):
        return u"%s" % self.name

    def teacher_list(self):
        return ','.join(i.name for i in self.teacher.all())

    class Meta:
        db_table = "class_room"
        ordering = ('created_at',)
        verbose_name = "班级管理"
        verbose_name_plural = "班级管理"


class StudentModel(models.Model):
    name = models.CharField(u'学生姓名', max_length=64)
    pwd = models.CharField(u'学生密码', max_length=64)
    grade = models.CharField(u'年级', max_length=64)
    mobile = models.IntegerField(u'手机号码')
    school = models.CharField(u'学校', max_length=64)
    class_room = models.ForeignKey(ClassRoom, on_delete=models.CASCADE, verbose_name='班级名称')
    parent_name = models.CharField(u'家长姓名', max_length=64)
    created_at = models.DateTimeField(u'创建时间', auto_now_add=True)
    updated_at = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return u"%s" % self.name

    def __str__(self):
        return u"%s" % self.name

    class Meta:
        db_table = "studen"
        ordering = ('created_at',)
        verbose_name = "学生管理"
        verbose_name_plural = "学生管理"
