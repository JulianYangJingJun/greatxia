from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin
from django.http import JsonResponse
from simpleui.admin import AjaxAdmin
from django import forms


from institution.models import Teacher, ClassRoom, CurriculumModel, StudentModel
from institution.forms import ClassRoomForm


class CurriculumAdmin(admin.ModelAdmin):
    list_display = ('title', 'image_data', 'suitable_people', 'course_plan', 'course_tools',
                    'course_number', 'course_sections', 'created_at', 'updated_at')
    search_fields = ('title', 'course_tools', 'suitable_people', 'course_tools')
    fields = ('title', 'cover_pic', 'suitable_people', 'course_plan', 'course_tools',
              'course_number', 'course_sections')


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('name', 'mobile', 'is_state', 'created_at', 'updated_at')
    search_fields = ('name', 'mobile', 'is_state')
    fields = ('name', 'mobile', 'is_state')


class ClassRoomAdmin(AjaxAdmin):
    list_display = ('name', 'restrict', 'invitation_code', 'teacher_list', 'is_audit', 'created_at', 'updated_at')
    search_fields = ('name', 'invitation_code', 'is_audit')
    fields = ('name', 'restrict', 'invitation_code', 'teacher', 'is_audit')
    # 增加自定义按钮
    actions = ['batch_input', 'curriculum_button', 'coupon_button']

    def curriculum_button(self, request, queryset):
        pass

    curriculum_button.short_description = '一键授权课程'
    curriculum_button.icon = 'fas fa-solid fa-book'
    curriculum_button.type = 'primary'

    def coupon_button(self, request, queryset):
        pass

    # 显示的文本，与django admin一致
    coupon_button.short_description = '一键授权试卷'
    # icon，参考element-ui icon与https://fontawesome.com
    coupon_button.icon = 'fas fa-audio-description'

    # 指定element-ui的按钮类型，参考https://element.eleme.cn/#/zh-CN/component/button
    coupon_button.type = 'primary'

    # 给按钮追加自定义的颜色
    # coupon_button.style = 'color:black;'

    def batch_input(self, request, queryset):
        # 这里的queryset 会有数据过滤，只包含选中的数据

        post = request.POST
        # 这里获取到数据后，可以做些业务处理
        # post中的_action 是方法名
        # post中 _selected 是选中的数据，逗号分割
        if not post.get('_selected'):
            return JsonResponse(data={
                'status': 'error',
                'msg': '请先选中数据！'
            })
        else:
            return JsonResponse(data={
                'status': 'success',
                'msg': '处理成功！'
            })

    batch_input.short_description = '批量添加'
    batch_input.type = 'success'
    batch_input.icon = 'el-icon-plus'

    # 指定一个输入参数，应该是一个数组

    # 指定为弹出层，这个参数最关键
    batch_input.layer = {
        # 弹出层中的输入框配置

        # 这里指定对话框的标题
        'title': '弹出层输入框',
        # 提示信息
        # 'tips': '这个弹出对话框是需要在admin中进行定义，数据新增编辑等功能，需要自己来实现。',
        # 确认按钮显示文本
        'confirm_button': '确认提交',
        # 取消按钮显示文本
        'cancel_button': '取消',

        # 弹出层对话框的宽度，默认50%
        'width': '40%',

        # 表单中 label的宽度，对应element-ui的 label-width，默认80px
        'labelWidth': "80px",
        'params': [{
            # 这里的type 对应el-input的原生input属性，默认为input
            'type': 'input',
            # key 对应post参数中的key
            'key': 'name',
            # 显示的文本
            'label': '名称',
            # 为空校验，默认为False
            'require': True
        }, {
            'type': 'select',
            'key': 'type',
            'label': '类型',
            'width': '200px',
            # size对应elementui的size，取值为：medium / small / mini
            'size': 'small',
            # value字段可以指定默认值
            'value': '0',
            'options': [{
                'key': '0',
                'label': '收入'
            }, {
                'key': '1',
                'label': '支出'
            }]
        }, {
            'type': 'number',
            'key': 'money',
            'label': '金额',
            # 设置默认值
            'value': 1000
        }, {
            'type': 'date',
            'key': 'date',
            'label': '日期',
        }, {
            'type': 'datetime',
            'key': 'datetime',
            'label': '时间',
        }, {
            'type': 'rate',
            'key': 'star',
            'label': '评价等级'
        }, {
            'type': 'color',
            'key': 'color',
            'label': '颜色'
        }, {
            'type': 'slider',
            'key': 'slider',
            'label': '滑块'
        }, {
            'type': 'switch',
            'key': 'switch',
            'label': 'switch开关'
        }, {
            'type': 'input_number',
            'key': 'input_number',
            'label': 'input number'
        }, {
            'type': 'checkbox',
            'key': 'checkbox',
            # 必须指定默认值
            'value': [],
            'label': '复选框',
            'options': [{
                'key': '0',
                'label': '收入'
            }, {
                'key': '1',
                'label': '支出'
            }, {
                'key': '2',
                'label': '收益'
            }]
        }, {
            'type': 'radio',
            'key': 'radio',
            'label': '单选框',
            'options': [{
                'key': '0',
                'label': '收入'
            }, {
                'key': '1',
                'label': '支出'
            }, {
                'key': '2',
                'label': '收益'
            }]
        }]
    }


class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'pwd', 'grade', 'mobile', 'school', 'class_room', 'parent_name', 'created_at', 'updated_at')
    search_fields = ('name', 'mobile', 'grade')
    fields = ('name', 'pwd', 'grade', 'mobile', 'school', 'class_room', 'parent_name', )


admin.site.register(StudentModel, StudentAdmin)
admin.site.register(CurriculumModel, CurriculumAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(ClassRoom, ClassRoomAdmin)
