from django import forms
from institution.models import ClassRoom


class ClassRoomForm(forms.ModelForm):
    is_audit = forms.CheckboxInput()

    class Meta:
        forms.models = ClassRoom
